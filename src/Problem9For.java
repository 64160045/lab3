import java.util.Scanner;

public class Problem9For {
    public static void main(String[] args) {
        int number = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Input Number : ");
        number = sc.nextInt();
        for (int i = 1; i <= number; i++) {
            for (int j = 1; j <= number; j++) {
                System.out.print(j);
            }
            System.out.println();
        }

    }
}